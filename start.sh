#!/usr/bin/env bash

## launch echo_server
echo_server &
echo "- started echo_server"

## launch echo_gateway
sleep 1
echo_gateway &
echo "- started echo_gateway"

## To keep running the service
tail -f /dev/null
