.PHONY: all build clean build_pb distclean_pb

APP := echo
PROTO := ./apps/pb/echo.proto

all: build_pb build
build:
	(cd apps/gateway/ && go build -o $(APP)_gateway && mv ./$(APP)_gateway ../../bin/)
	(cd apps/server/ && go build -o $(APP)_server && mv ./$(APP)_server ../../bin/)
clean:
	(rm bin/*)

## For gRPC
build_pb:
	protoc -I/usr/local/include -I. \
	        -I${GOPATH}/src \
	        -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
	        --swagger_out=logtostderr=true:. \
	        --grpc-gateway_out=logtostderr=true:. \
		--go_out=Mgoogle/api/annotations.proto=github.com/gengo/grpc-gateway/third_party/googleapis/google/api,plugins=grpc:. \
	        $(PROTO)
distclean_pb:
	(rm ./apps/pb/*.go ./apps/pb/*.json)
