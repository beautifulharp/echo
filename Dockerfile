##
## RIT Technology Platform
##
## A API-Gateway Image of Echo
##
FROM skylabs/base:0.3

ENV LANG=en_US.UTF-8 TERM=xterm
ENV GOROOT /usr/lib/go
ENV GOPATH /go
ENV GOBIN /go/bin
ENV PATH $PATH:$GOROOT/bin:$GOPATH/bin

ENV SERVICE_NAME echo
ENV GIT_URL https://bitbucket.org/beautifulharp/echo.git


##
## Installing dependencies
##
RUN mkdir -p ${HOME} && \
    apk --no-cache --update upgrade && \
    apk add --no-cache \
        autoconf \
        automake \
        build-base

## Building the component
RUN cd ${HOME} \
    && git clone ${GIT_URL} \
    && cd ${SERVICE_NAME} \
    && make \
    && cp ./bin/* /usr/local/bin/ \
    && mkdir -p /var/local/skymatrix/pb/ \
    && cp ./apps/pb/*.json /var/local/skymatrix/pb/ \
    && cp ./apps/pb/*.proto /var/local/skymatrix/pb/ \
    && ls -lR /var/local/skymatrix/

## The gateway's port number
EXPOSE 8888

## Executing 'start.sh'
CMD ["sh", "/root/echo/start.sh"]
