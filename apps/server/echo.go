//
// RIT Tech Platform - Sample Componnet (1)
//
//
package main

import (
	// "fmt"
	"os"

	"github.com/golang/glog"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	pb "../pb"
)

// Implements of EchoServiceServer

type echoServer struct{}

func newEchoServer() pb.EchoServer {
	return new(echoServer)
}

// Method-1:
func (s *echoServer) Echo(ctx context.Context, msg *pb.Message) (*pb.Message, error) {
	glog.Info(msg)

	// fmt.Print("OK:echo_1, ")
	// fmt.Println(msg)
	return msg, nil
}

// Method-2:
func (s *echoServer) EchoBody(ctx context.Context, msg *pb.Message) (*pb.Message, error) {
	glog.Info(msg)

	// fmt.Print("OK:echo_2, ")
	// fmt.Println(msg)

	grpc.SendHeader(ctx, metadata.New(map[string]string{
		"foo": "foo1",
		"bar": "bar1",
	}))
	grpc.SetTrailer(ctx, metadata.New(map[string]string{
		"foo": "foo2",
		"bar": "bar2",
	}))
	return msg, nil
}

// Method-3:
func (s *echoServer) Hostname(ctx context.Context, msg *pb.Message) (*pb.Message, error) {
	glog.Info(msg)

	// fmt.Print("OK:hostanme, ")
	hostname, _ := os.Hostname()
	// fmt.Println(hostname)

	out := new(pb.Message)
	out.Hostname = hostname
	return out, nil
}
